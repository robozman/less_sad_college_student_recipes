Tak Pasta
=========

Nice easy and quick pasta dish with ingredients you can keep in the cabinet/fridge.
The key to making this dish sucessfully is timing the sauce and pasta such that they
are done at the same time. I'll try to give some guidelines throughout but the best
way to master the timing is to make the recipe a few times.


Ingredients
-----------
 - Fennel Seeds
 
 - 3 Cloves Garlic
 
 - 1/2 Onion
 
 - Sundried Tomatos
 
 - Capers
 
 - Chili Flakes
 
 - 1/2 TBSP Butter
 
 - Oregano (to taste)
 
 - Pasta (Linguini, Fettuccine, Thick Spaghetti)
      
 - Cooking oil (Olive, Canola, a mix)


Prep
----
Stove/Oven
__________

Sauce
_______________
 1. Chop garlic finely
 
 2. Dice onion
 
 3. Chop capers finely
 
 4. Chop sundried tomatoes into small pieces 

Pasta
_____
 1. Fill pot with water
 
 2. Salt water (like the sea)

Cooking
-------
Sauce
_____
 1. Add oil to pan and heat until shimmering
  
 2. Add fennel seeds to pan
  
 3. Wait 30 seconds
  
 4. Add onion and garlic to pan
  
 5. Wait 30 seconds

 6. Add capers and sundried tomatoes to pan

 7. When pasta is 1/2 done, add 1 ladleful of pasta water to sauce

 8. Add oregano to taste 

Pasta
_____

 1. Boil water

 2. Add pasta to water

 3. Right before the pasta is al dente
     1. Save some of the pasta water on the side

     2. Move pasta to sauce pan

 4. Saute pasta with sauce until cooked to your taste
