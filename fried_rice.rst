Fried Rice
==========

Ingredients
-----------
    - 1 Cup Brown Rice

    - 2 Carrots

    - 2 Heads Brocoli

    - 1.5 Handfuls Frozen Peas

    - 2 eggs (optional)

    - Sriracha

    - Salt and Pepper

    - Method to steam vegtables

    - Method to cook rice

    - Canola oil (or other neturally tasting oil)

    - Sesame oil (optional)



Brown Rice (boil)
_________________
    1. Measure out 1 cup of rice and add to aparatus.

    2. Add enough water to cover the rice.
           
    3. Use your hand to mix the rice and water to wash the starch off the rice. Drain water and repeat.

    4. Add 1 and 2/3 cup of water to the rice.

    5. Salt the rice to taste. 

    6. Add the lid and place the pot on the stove. 

    7. Set the stove to high.

    8. Let the rice cook on high until the water comes to a boil. Then lower the heat to low. 

    9. Let the rice cook until the texture seems right (brown rice can take awhile). 

    
Vegetables
__________
    1. Cut the carrots into circular slices about 1/4 - 1/2 inches wide. 
           
    2. Cut the broccoli, removing most of the stem. You should end up with little pieces. 

    3. Steam the carrots. I typically boil water and use a pot with a steamer and a lid.

    4. Remove the carrots when they are tender (taste one).

    5. Steam the broccoli. 

    6. Remove when tender (taste one).


Fried Rice
__________
    1. Add a few tablespoons of canola oil to a pan or wok 

    2. Add a small amount of sesame oil to the pan

    3. Bring up to medium heat

    4. Once the pan is up to heat, add the vegtables to the pan

    5. Let cook for a bit, then add two eggs to the pan. 

    6. Scramble the eggs, spreading them throughout the vegtables. 

    7. Saute the vegtables and egg, adding salt, pepper, soy sauce, and sriracha to taste.

    8. Add the brown rice to the pan. Increase the heat a small bit.

    9. Season the entire mix to taste. Stir the contents of the pan to spread the seasoning to all parts.

    10. Remove from pan when done cooking (taste it). Should only take a few minuties. 
