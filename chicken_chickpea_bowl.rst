Chicken Chickpea Bowl
=====================

Ingredients
-----------
    - 3 Chicken Thighs (Fresh or Frozen)
    
    - 1 Can Chickpeas (Garbanzo Beans)

    - 1/4 Yellow Onion

    - 1 Sweet Potatoe

    - 1 Cup Brown Rice

    - 2 Tablespoons Wasabi Mayo

    - 3/2 Tablespoons Siracha

    - Red Pepper Flakes

    - Olive Oil

    - Salt and Pepper

Components
----------

Chicken
_______
    1. Preheat oven to 375

    2. Dice onion finely

    3. Place chicken thighs in baking dish

    4. Put onion in dish

    5. Douse in olive oil

    6. Bake 25-35 min or until properly cooked (50-60 if frozen)

    7. When done, take chicken out of oil mixture (saving the mixture) and slice into small pieces

Brown Rice
__________
    1. Put 1 cup (185 grams) of brown rice into a small sauce pan

    2. Fill the pot with water, running your hands through the rice to clean the starch off. Then drain the water out.

    3. Repeat the last step.
           
    4. Fill the pot with 1 2/3 cups of water

    5. Put on stove covererd on high heat

    6. When rice comes to a boil, lower to low. Cook on low for 40-60 min (until soft)

Sweet Potatoes
______________
    1. Rinse potatoe in the sink under cold water 

    2. Optional: Peel the sweet potatoes if you do not like eating the skin

    3. Chop into very thin disks

    4. Lay disks on baking tray, dousing in olive oil

    5. Place in oven with chicken, baking 20-25 min (until tender)

Chickpeas
_________
    1. In the sink, empty can of chickpeas into strainer

    2. Run cold water over the chickpeas, running your hand through them to rinse them off

Combination
-----------
    1. Wait until all components are done    

    2. Drain oilve oil and onion mix from chicken into skillet
           
    3. Add oilve oil and needed to coat bottom of pan

    4. Heat up pan on medium heat

    5. Add wasabi mayo, siracha, and red pepper flakes to olive oil, stirring in to combine.
           
    6. As you go through the cooking process, taste the mixture and add more wasabi mayo and siracha as needed
    
    7. Add chickpeas to pan, sauteing until warm. 

    8. Add brown rice to pan (quantity is up to you, maybe 1/2 of what you made)

    9. Add sweet potato slices to pan. Don't worry if they have gotten hard, they will soften up in the pan

    10. Add sliced chicken to pan, stirring to combine. 

    11. When all components are sufficiently heated, transfer to bowl to serve
