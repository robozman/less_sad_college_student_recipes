Shepards Pie
============

Ingredients
-----------
    - 3 Russet Potatos
    
    - Milk

    - Butter

    - 1lb Ground Beef

    - 2 Carrots

    - 5 Large Shiitake Mushrooms

    - 1 Can Sweet Peas / 1/4 Bag Frozen Peas

    - 1 Can Sweet Corn

    - 2 Cloves Garlic

    - 1 Can Vegetable Broth (Can use chicken broth, but I find the flavor dominating)

    - Flour

    - Worcestershire Sauce

    - Salt and Pepper

Prep
----

Mashed Potatoes
_______________
    1. Chop potates into 1in chunks

    2. Find pot with steamer top and lid

    3. Put water in pot

Mince
_____
    1. Chop carrots into matchsticks

    2. Cut mushrooms into strips (2 in x 0.5in)

    3. Wash and strain canned peas and carrots
           
    4. Wisk broth and flour together to make airy mixture (I sometimes skip whisking the mixture when I'm lazy)

    5. Chop or press garlic into small bits

Cooking
-------
Mashed Potatoes
_______________
    1. Steam potatoes until a toothpick passes through them easily

    2. Mash potatoes until they are to your liking, adding butter, milk, and salt as needed

    3. Black pepper and chopped green onions can be added as well

Stove/Oven
__________
    1. Preheat oven on broil

Mince
_____
    1. Add oil to pan and heat until shimmering

    2. Brown ground beef in pan/wok, adding salt and pepper as needed

    3. When done, remove ground beef from pan and strain

    4. Without cleaning the pan, add addition oil

    5. Add garlic to pan and saute for 30 seconds

    6. Add carrots to pan and let cook for 3-5 minuites

    7. Add mushrooms to pan

    8. Season with salt and pepper

    9. Add lid and let cook until vegtables are 3/4 of the way done

    8. Added peas and corn to mix

    9. Season with salt and pepper
    
    10. Add lid and let cook until mostly done

    11. Add ground beef and broth + flour mix to pan

    12. Let cook down, stirring regularaly

    13. Add seasoning and broth as needed

    14. When cooked down, remove from pan and place in baking dish

    15. Spread evenly throughout dish

    16. Place mashed potatoes on top of mince, spreading evenly

    17. Run prongs of fork over top of mashed potatoes to create channels

    18. Put in oven for 10-15 min

    19. Remove from over and let stand about 10-15 min so you don't burn your tounge

